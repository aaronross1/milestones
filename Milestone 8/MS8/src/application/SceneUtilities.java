/**
 * 
 */
package application;

import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;
import ms8.Product;

/**
 * class that houses headers and cart animation effect
 * @author Aaron W Ross
 *
 */
public class SceneUtilities {

	Stage primaryStage;
	Product [] products;
	
	/**
	 *  Non-default constructor for SceneUtilities
	 * @param primaryStage
	 * @param products
	 */
	public SceneUtilities(Stage primaryStage, Product[] products) {
		this.primaryStage = primaryStage;
		this.products = products;

	}

	/**
	 * Header with home button and header title
	 * @return
	 */
	public HBox header() {
		Button btn1 = new Button("Home");
		btn1.setStyle("-fx-background-color: white");
		btn1.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.REGULAR, 30));
		btn1.setOnAction(new ButtonClickHandlerHome());

		Text header = new Text();
		header.setText("Select a product");
		header.setFill(Color.WHITE);
		header.setFont(Font.font("Calibri", FontWeight.BOLD, FontPosture.REGULAR, 50));

		HBox hbox = new HBox();
		hbox.setPadding(new Insets(0, 0, 0, 120));
		hbox.getChildren().addAll(btn1, header);
		hbox.setSpacing(50);
		hbox.setAlignment(Pos.TOP_LEFT);
		return hbox;

	}

	/**
	 * Admin header for the restock page
	 * @return
	 */
	public HBox adminHeaderRestock() {
		Button btn1 = new Button("Home");
		btn1.setStyle("-fx-background-color: white");
		btn1.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.REGULAR, 30));
		btn1.setOnAction(new ButtonClickHandlerHome());
		
		Text header = new Text();
		header.setText("Restock Ticket");
		header.setFill(Color.WHITE);
		header.setFont(Font.font("Calibri", FontWeight.BOLD, FontPosture.REGULAR, 50));
		
		HBox hbox = new HBox();
		hbox.setPadding(new Insets(0, 0, 0, 120));
		hbox.getChildren().addAll(btn1, header);
		hbox.setSpacing(50);
		hbox.setAlignment(Pos.TOP_LEFT);
		
		return hbox;
	}
	
	/**
	 * Returns an hbox with the header for the admin scene
	 * @return 
	 * 
	 */
	public VBox adminHeader() {
		Button btn1 = new Button("Home");
		btn1.setStyle("-fx-background-color: white");
		btn1.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.REGULAR, 30));
		btn1.setOnAction(new ButtonClickHandlerHome());

		Button btn2 = new Button("Restock All");
		btn2.setStyle("-fx-background-color: white");
		btn2.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.REGULAR, 20));
		btn2.setOnAction(new RestockHandler(btn2));

		Text header = new Text();
		header.setText("Admin Panel");
		header.setFill(Color.WHITE);
		header.setFont(Font.font("Calibri", FontWeight.BOLD, FontPosture.REGULAR, 50));

		HBox hboxTop = new HBox();
		hboxTop.setPadding(new Insets(0, 0, 0, 120));
		hboxTop.getChildren().addAll(btn1, header, btn2);
		hboxTop.setSpacing(50);
		hboxTop.setAlignment(Pos.TOP_LEFT);
		
		
		Button btnSortStock = new Button("Sort by Stock");
		btnSortStock.setStyle("-fx-background-color: white");
		btnSortStock.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.REGULAR, 20));
		btnSortStock.setOnAction(new ButtonClickSortStock());

		Button btnSortName = new Button("Sort by Name");
		btnSortName.setStyle("-fx-background-color: white");
		btnSortName.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.REGULAR, 20));
		btnSortName.setOnAction(new ButtonClickSortName());
		
		final TextField search = new TextField();
		search.setPromptText("Search");
		search.setPrefColumnCount(10);
		search.addEventHandler(KeyEvent.KEY_PRESSED, event -> new ButtonClickHandlerSearch(search));
		
		
		HBox hboxBottom = new HBox();
		hboxBottom.setPadding(new Insets(15, 0, 0, 20));
		hboxBottom.getChildren().addAll(btnSortStock, btnSortName, search);
		hboxBottom.setSpacing(50);
		hboxBottom.setAlignment(Pos.TOP_RIGHT);
		
		VBox vbox = new VBox();
		vbox.setPadding(new Insets(0, 20, 20, 0));
		vbox.getChildren().addAll(hboxTop, hboxBottom);
		return vbox;

	}

	/**
	 * checkout button and cart image
	 * 
	 * @return
	 */
	public HBox checkoutFooter() {

		Image cart = new Image("./images/cart.png");
		ImageView imageView = new ImageView(cart);
		imageView.setFitWidth(50);
		imageView.setFitHeight(50);

		Button btn2 = new Button("Cancel Selection");
		btn2.setStyle("-fx-background-color: white");
		btn2.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.REGULAR, 20));
		btn2.setOnAction(new CancelSelection());
		
		Button btn1 = new Button("Checkout");
		btn1.setStyle("-fx-background-color: white");
		btn1.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.REGULAR, 20));
		btn1.setOnAction(new CartEffect(imageView));

		HBox hbox = new HBox();
		hbox.setPadding(new Insets(20, 60, 30, 0));
		hbox.getChildren().addAll(btn2, btn1, imageView);
		hbox.setSpacing(20);
		hbox.setAlignment(Pos.BOTTOM_RIGHT);
		return hbox;
	}

	/**
	 * Sort function for sorting the array of products by stock or by name
	 * @param arr
	 * @param n
	 * @param flag 
	 */
	public void sort(Product[] arr, int n, int flag) {
		if (n == 1)
			return;
		if (flag == 1) {
		for(int i=0; i<n-1; i++)
		{
			if (arr[i].getStock() > arr[i+1].getStock())
			{
				Product temp = arr[i];
				arr[i] = arr[i+1];
				arr[i+1] = temp;
			}
		}
		
		sort(arr, n-1, 1);
		}
		
		if (flag ==2) {
			for(int i=0; i<n-1; i++)
			{
				if (arr[i].getName().compareTo(arr[i+1].getName()) > 0)
				{
					Product temp = arr[i];
					arr[i] = arr[i+1];
					arr[i+1] = temp;
				}
			}
			
			sort(arr, n-1, 2);
		}
	}
	
	/**
	 * Returns a true or false value based on whether product needs restocked
	 * @param p
	 * @return
	 */
	public boolean lowStockFlag(Product p)
	{
		if (p.getLowQuantity() >= p.getStock())
		{
			return true;
		}
		else {
			return false;
		}
	}
	
	/**
	 * Restock calculation for cost of restocking products
	 * @param p
	 * @return
	 */
	public float restockPriceCalculation(Product p) 
	{
		//Number of products to restock
		float restockProducts = 20;
		float cost;
		restockProducts = restockProducts - p.getStock();
		cost = restockProducts * p.getPrice() * (float)0.5;
		return cost;
		
	}
	
	/**
	 * Restock product calculation for number of products being restocked
	 * @param p
	 * @return
	 */
	public int restockProductCalculation(Product p) 
	{
		//Number of products to restock
		int restockProducts = 20;
		restockProducts = restockProducts - p.getStock();
		return restockProducts;
		
	}
	

	/**
	 * Button click handler for search box
	 * @author Aaron W Ross
	 *
	 */
	class ButtonClickHandlerSearch {

		public ButtonClickHandlerSearch(TextField search) {
			search.addEventFilter(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
				public void handle(KeyEvent a) {
					if (a.getCode() == KeyCode.ENTER) {
						String input = search.getText();
						for(int i = 0; i < products.length; i++)
						{
							if(input.contains(products[i].getName())) {
							AdminScene as = new AdminScene(primaryStage, products);
							as.displaySearch(products[i]);
							}
						}
						

					}

				}
			});
		}
	}
	/**
	 * Button click handler for the sort by stock. 
	 * @author Aaron W Ross
	 *
	 */
	class ButtonClickSortStock implements EventHandler<ActionEvent> {
		AdminScene as = new AdminScene(primaryStage, products);
		
		@Override
		public void handle(ActionEvent e) {
			sort(products, products.length, 1);
			as.display();
		}
	}
	
	/**
	 * Button click handler for the sort by stock. 
	 * @author Aaron W Ross
	 *
	 */
	class ButtonClickSortName implements EventHandler<ActionEvent> {
		AdminScene as = new AdminScene(primaryStage, products);
		
		@Override
		public void handle(ActionEvent e) {
			sort(products, products.length, 2);
			as.display();
		}
	}
	
	/**
	 * Button click handler for the home button to go home. 
	 * @author Aaron W Ross
	 *
	 */
	class ButtonClickHandlerHome implements EventHandler<ActionEvent> {
		@Override
		public void handle(ActionEvent e) {
			MainScene ms = new MainScene(primaryStage, products);
			ms.display();
		}
	}

	/**
	 * event handler for animation effect for cart
	 * 
	 * @author Aaron W Ross
	 *
	 */
	class CartEffect implements EventHandler<ActionEvent> {

		private ImageView imageView;

		/**
		 * Click handler effect for the cart imageView
		 * @param imageView
		 */
		public CartEffect(ImageView imageView) {
			this.imageView = imageView;
		}

		/**
		 * handler for the action of "bouncing" the cart
		 */
		@Override
		public void handle(ActionEvent e) {

			TranslateTransition bounce = new TranslateTransition(Duration.millis(50), imageView);
			bounce.setFromX(0f);
			bounce.setByX(10F);
			bounce.setCycleCount(4);
			bounce.setAutoReverse(true);
			bounce.play();
		}
	}
	
	/**
	 * event handler for restock button and effect
	 * 
	 * @author Aaron W Ross
	 *
	 */
	class RestockHandler implements EventHandler<ActionEvent> {

		private Button btn2;

		/**
		 * Click handler effect for the restock button
		 * @param btn2 
		 * @param imageView
		 */
		public RestockHandler(Button btn2) {
			this.btn2 = btn2;
		}

		/**
		 * handler for the action of "bouncing" the cart
		 */
		@Override
		public void handle(ActionEvent e) {
			AdminScene as = new AdminScene(primaryStage, products);
			as.displayRestock();
			
			for (int i = 0; i < products.length; i++) {
				if (products[i].getStock() < 20) {
				products[i].setStock(20);
				}
			}
			
			TranslateTransition bounce = new TranslateTransition(Duration.millis(50), btn2);
			bounce.setFromX(0f);
			bounce.setByX(10F);
			bounce.setCycleCount(4);
			bounce.setAutoReverse(true);
			bounce.play();
		}
	}

	/**
	 * event handler for restock button and effect
	 * 
	 * @author Aaron W Ross
	 *
	 */
	class CancelSelection implements EventHandler<ActionEvent> {
		/**
		 * handler for the action of "bouncing" the cart
		 */
		@Override
		public void handle(ActionEvent e) {

			for (int i = 0; i < products.length; i++) {
				if (products[i].getStock() < 20) {
				products[i].setStock(20);
				}
			}

		}
	}
}
