/**
 * 
 */
package application;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.LinkedList;
import java.util.Queue;

import ms8.Drink;
import ms8.Product;

/**
 * 
 * @author Aaron W Ross
 * @param <T> 
 *
 */
public class ProcessCustomerQueue<T> {

	Queue<Order> customerQueue = new LinkedList<Order>();
	
	/**
	 * initializing the queue
	 * @param products 
	 * @param app 
	 */
	public void initQueue(Product[] products, App app) {
		
		try {
			  String line = "";
			  BufferedReader b = new BufferedReader(new FileReader("./productQueue.txt"));
			  while((line = b.readLine()) != null)
				{
				String[] tokens = line.split("\\|");
				tokens[0] = tokens[0].trim();
				tokens[1] = tokens[1].trim();

				String product = tokens[0];
				String name = tokens[1];
				
				Order queueObject = new Order(name, product);
				
				customerQueue.add(queueObject);
				app.getDispenser().initProducts();
				}
		}
		catch (IOException e) {
			  e.printStackTrace();
		  }
	}
	
	
	/**
	 * Just looking at the first object
	 * @param q
	 * @return
	 */
	public Order first() {
		return customerQueue.peek();
	}
	
	/**
	 * Getting the length of the queue
	 * @param q
	 * @return
	 */
	public int length() {
		return customerQueue.size();
	}
	
	/**
	 * putting an object in the queue
	 * @param q
	 * @param e
	 */
	public void in(Order e) {
		customerQueue.add(e);
	}
	
	/**
	 * Taking the first object out of the queue
	 * @param q
	 * @return
	 */
	public Order out() {
		return customerQueue.poll();
	}
	
	/**
	 * Checking to see if the queue is empty
	 * @return
	 */
	public boolean empty() {
		if (customerQueue.peek().equals(null)) {
			return true;
		}
		else {
			return false;
		}
	}
	
	/**
	 * Getter for the queue
	 * @return
	 */
	public Queue getQueue() {
		return customerQueue;
	}
}
