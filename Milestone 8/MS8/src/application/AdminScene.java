/**
 * 
 */
package application;

import java.text.DecimalFormat;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import ms8.Product;

/**
 * Admin scene class for the admin control panel
 * @author Aaron W Ross
 *
 */
public class AdminScene {

	Stage primaryStage;
	Product[] products;
	SceneUtilities su;

	/**
	 * AdminScene non-default constructor
	 * @param primaryStage 
	 * @param products 
	 * @param root
	 */
	public AdminScene(Stage primaryStage, Product[] products) {
		this.su = new SceneUtilities(primaryStage, products);
		this.primaryStage = primaryStage;
		this.products = products;
	}

	/**
	 * Display method for the admin scene
	 */
	public void display() {
		BorderPane root = new BorderPane();
		Scene scene = new Scene(root, 900, 900);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		root.setStyle("-fx-background-color: lightblue");

		// home button and all products
		root.setTop(su.adminHeader());

		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(10);
		grid.setVgap(10);

		
		int row = 0;
		int column = 0;
		for (int i = 0; i < products.length; i++) {
			ProductView pv = new ProductView(products[i]);
			grid.add(pv.showProduct("admin"), column, row);
			column++;
			if (column == 3) {
				column = 0;
				row++;
			}
		}

		ScrollPane scroll = new ScrollPane(grid);
		scroll.setStyle("-fx-background-color: lightblue");
		scroll.setFitToHeight(true);
		scroll.setFitToWidth(true);
		BorderPane.setAlignment(grid, Pos.CENTER);
		root.setCenter(scroll);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	
	/**
	 * Display method for the search results
	 * @param product 
	 */
	public void displaySearch(Product product) {
		BorderPane root = new BorderPane();
		Scene scene = new Scene(root, 900, 900);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		root.setStyle("-fx-background-color: lightblue");

		// home button and header
		root.setTop(su.adminHeader());

		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(10);
		grid.setVgap(10);

		
		int row = 0;
		int column = 0;
		ProductView pv = new ProductView(product);
		grid.add(pv.showSearch(product), column, row);
			column++;
		if (column == 3) {
		column = 0;
		row++;
			}

		ScrollPane scroll = new ScrollPane(grid);
		scroll.setStyle("-fx-background-color: lightblue");
		scroll.setFitToHeight(true);
		scroll.setFitToWidth(true);
		BorderPane.setAlignment(grid, Pos.CENTER);
		root.setCenter(scroll);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	/**
	 * Restock screen for the admin panel
	 */
	public void displayRestock() {
		BorderPane root = new BorderPane();
		Scene scene = new Scene(root, 900, 900);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		root.setStyle("-fx-background-color: lightblue");

		// home button and all products
		root.setTop(su.adminHeaderRestock());

		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(10);
		grid.setVgap(10);

		float totalPrice = 0;
		int totalProducts = 0;
		int row = 0;
		int column = 0;
		for (int i = 0; i < products.length; i++) {
			if(su.lowStockFlag(products[i]) == true)
			{
			totalPrice += su.restockPriceCalculation(products[i]);
			totalProducts += su.restockProductCalculation(products[i]);
			ProductView pv = new ProductView(products[i]);
			grid.add(pv.showProduct("admin"), column, row);
			column++;
			if (column == 3) {
				column = 0;
				row++;
			}
			}
		}

		Text footer = new Text();
		DecimalFormat df = new DecimalFormat("#.00");
		footer.setText(" A total of " + totalProducts + " products with a total price of $" + df.format(totalPrice) + " have been ordered.");
		footer.setFill(Color.WHITE);
		footer.setFont(Font.font("Calibri", FontWeight.BLACK, FontPosture.REGULAR, 30));
		BorderPane.setAlignment(footer, Pos.CENTER);
		root.setBottom(footer);
		
		ScrollPane scroll = new ScrollPane(grid);
		scroll.setStyle("-fx-background-color: lightblue");
		scroll.setFitToHeight(true);
		scroll.setFitToWidth(true);
		BorderPane.setAlignment(grid, Pos.CENTER);
		root.setCenter(scroll);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
}

