package application;

import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.util.Duration;
import ms8.Product;

/**
 * Display product class which extends vbox
 * 
 * @author Aaron W Ross
 *
 */
public class ProductView {
	private Product product;
	
	/**
	 * Display product non-default constructor
	 * 
	 * @param product
	 */
	public ProductView(Product product) {
		this.product = product;

	}

	/**
	 * show product method which is a vbox. Displays product and info in a vbox
	 * @param type 
	 * @return
	 */
	public VBox showProduct(String type) {
		VBox vb = new VBox();
		
		Button btn1 = new Button("Select");
		btn1.setStyle("-fx-background-color: grey");
		btn1.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.REGULAR, 20));
		btn1.setOnAction(new ButtonClickHandlerSelect(btn1));
		
		//if product is out of stock, display button that says it's out of stock
		if(product.getStock() < 1) {
			btn1.setText("OUT OF STOCK");
			btn1.setOnAction(new OutOfStockEffect(btn1));
		}

		//image for the product
		Image productImage = new Image(product.getImage());
		ImageView imageView1 = new ImageView(productImage);
		imageView1.setFitWidth(100);
		imageView1.setFitHeight(100);

		//description for the product
		Text description = new Text(product.getDescription());
		description.setFill(Color.BLACK);
		description.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.REGULAR, 14));

		//price of the product with the toString method for formatting
		Text price = new Text(product.toString());
		price.setFill(Color.BLACK);
		price.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.REGULAR, 14));

		//Stock of the product for display with the admin scene
		Text stock = new Text("Stock: " + product.getStock());
		price.setFill(Color.BLACK);
		price.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.REGULAR, 14));

		vb.setMinWidth(250);
		vb.setMaxWidth(250);
		vb.setPadding(new Insets(10, 10, 10, 10));
			if (type == "customer") {
				vb.getChildren().addAll(imageView1, description, price, btn1);
			} else if (type == "admin") {
				vb.getChildren().addAll(imageView1, description, price, stock);
			} 
		vb.setSpacing(10);
		vb.setAlignment(Pos.CENTER);
		vb.setStyle("-fx-background-color: white");
		return vb;

	}

	/**
	 * Show the product searched for
	 * @param product
	 * @return
	 */
	public VBox showSearch(Product product) {
	VBox vb = new VBox();
	
	//image for the product
	Image productImage = new Image(product.getImage());
	ImageView imageView1 = new ImageView(productImage);
	imageView1.setFitWidth(100);
	imageView1.setFitHeight(100);

	//description for the product
	Text description = new Text(product.getDescription());
	description.setFill(Color.BLACK);
	description.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.REGULAR, 14));

	//price of the product with the toString method for formatting
	Text price = new Text(product.toString());
	price.setFill(Color.BLACK);
	price.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.REGULAR, 14));

	//Stock of the product for display
	Text stock = new Text("Stock: " + product.getStock());
	price.setFill(Color.BLACK);
	price.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.REGULAR, 14));

	vb.setMinWidth(250);
	vb.setMaxWidth(250);
	vb.setPadding(new Insets(10, 10, 10, 10));
	vb.getChildren().addAll(imageView1, description, price, stock);
	vb.setSpacing(10);
	vb.setAlignment(Pos.CENTER);
	vb.setStyle("-fx-background-color: white");
	return vb;
	
	}
	
	/**
	 * Button click handler for the select button to go home.
	 * 
	 * @author Aaron W Ross
	 *
	 */
	class ButtonClickHandlerSelect implements EventHandler<ActionEvent> {
		private Button btn1;

		/**
		 * Click handler effect for the select button
		 * @param btn1 
		 * @param imageView
		 */
		public ButtonClickHandlerSelect(Button btn1) {
			this.btn1 = btn1;
		}
		
		/**
		 * Handler for select button
		 * Bounces if stock is 0
		 */
		@Override
		public void handle(ActionEvent e) {
			if (product.getStock() > 0) {
				product.setStock(product.getStock() - 1);
			}
			else {TranslateTransition bounce = new TranslateTransition(Duration.millis(50), btn1);
			bounce.setFromX(0f);
			bounce.setByX(10F);
			bounce.setCycleCount(4);
			bounce.setAutoReverse(true);
			bounce.play();
			}
		}
	}
	
	/**
	 * event handler for restock button and effect
	 * 
	 * @author Aaron W Ross
	 *
	 */
	class OutOfStockEffect implements EventHandler<ActionEvent> {

		private Button btn1;

		/**
		 * Click handler effect for the restock button
		 * @param btn1 
		 * @param imageView
		 */
		public OutOfStockEffect(Button btn1) {
			this.btn1 = btn1;
		}

		/**
		 * handler for the action of "bouncing" the button 
		 */
		@Override
		public void handle(ActionEvent e) {
			btn1.setText("OUT OF STOCK");
			TranslateTransition bounce = new TranslateTransition(Duration.millis(50), btn1);
			bounce.setFromX(0f);
			bounce.setByX(10F);
			bounce.setCycleCount(4);
			bounce.setAutoReverse(true);
			bounce.play();
		}
	}
}
