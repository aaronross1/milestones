/**
 * 
 */
package application;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import ms8.Product;

/**
 * @author Aaron W Ross
 *
 */
public class QueueScene {

	Stage primaryStage;
	Product[] products;
	ListView<String> customerList = new ListView<>();
	ProcessCustomerQueue queue = new ProcessCustomerQueue();

	/**
	 * MainScene non-default constructor
	 * @param primaryStage
	 * @param products
	 */
	public QueueScene(Stage primaryStage, Product[] products) {
		this.primaryStage = primaryStage;
		this.products = products;

	}
		/**
		 * Display method creating the queue scene
		 * @param app 
		 */
		public void display(App app) {
			BorderPane root = new BorderPane();
			Scene scene = new Scene(root, 800, 900);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());

			primaryStage.setTitle("Signature Snacks");
			root.setStyle("-fx-background-color: lightblue");

			//header
			Text header = new Text();
			header.setText("Signature Snacks");
			header.setFill(Color.WHITE);
			header.setFont(Font.font("Calibri", FontWeight.BOLD, FontPosture.REGULAR, 50));
			BorderPane.setAlignment(header, Pos.CENTER);
			root.setTop(header);

			//footer
			Text footer = new Text();
			footer.setText(" All products ordered from queue. ");
			footer.setFill(Color.WHITE);
			footer.setFont(Font.font("Calibri", FontWeight.BLACK, FontPosture.REGULAR, 40));
			BorderPane.setAlignment(footer, Pos.CENTER);
			root.setBottom(footer);
			
			// listview dynamically populated as it reads the information
			VBox vbox = new VBox();
			queue.initQueue(products, app);
			processOrders(products, vbox);
			
			
			vbox.getChildren().add(customerList);
			root.setCenter(customerList);
			primaryStage.setScene(scene);
			primaryStage.show();
		}
		
		/**
		 * Processing orders and subtracting stock by 1
		 * @param products
		 * @param vbox 
		 */
		public void processOrders(Product[] products, VBox vbox) {
			
			if(!queue.customerQueue.isEmpty()) {
			int length = queue.length();
			for(int i=0; i<length; i++) 
			{			
				float price = 0;
				String alternate = "";
				boolean orderFound = false;
				Order order = queue.out();
				// Looping through products to find a match for the searched for product
					for(int t = 0; t < products.length; t++)
					{
						if(products[t].getName().equals(order.getProduct())) 
						{
							products[t].setStock(products[t].getStock() - 1);
							price = products[t].getPrice();
							order.setAlternate(false);
							orderFound = true;
							
						}
					}
					if(orderFound == false)
						{
							products[1].setStock(products[1].getStock() - 1);
							price = products[1].getPrice();
							order.setAlternate(true);
						}
					if(order.getAlternate() == false) {
						alternate = "Product ordered";
						customerList.getItems().add(order.getName() + "   " + order.getProduct() + "  " + price + ". " + alternate);
					}
					else if(order.getAlternate() == true) {
						alternate = "Alternate product ordered";
						customerList.setStyle("-fx-text-fill:red;");
						customerList.getItems().add(order.getName() + "   " + order.getProduct() + "  " + price + ". " + alternate);
					}
					
				}
			}
		}
}
