/**
 * 
 */
package application;

/**
 * Order class with name and product for an order
 * @author Aaron W Ross
 *
 */
public class Order{	
	String name;
	String product;
	boolean alternate=false;
	
	
	/**
	 * Getting the alternate flag
	 * @return
	 */
	public boolean getAlternate() {
		return alternate;
	}

	/**
	 * Set the alternate flag
	 * @param isOrdered
	 */
	public void setAlternate(boolean isOrdered) {
		this.alternate = isOrdered;
	}

	/**
	 * Order constructor
	 * @param name
	 * @param product
	 */
	public Order(String name, String product) {
		this.name = name;
		this.product = product;
	}
	
	/**
	 * Getter for the name on the order
	 * @return
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Setter for the name on the order
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Getter for the product name
	 * @return
	 */
	public String getProduct() {
		return product;
	}
	
	/**
	 * Setter for the product name
	 * @param product
	 */
	public void setProduct(String product) {
		this.product = product;
	}
}
