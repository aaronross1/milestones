package application;
import javafx.animation.FadeTransition;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.util.Duration;
import ms8.Dispenser;
import ms8.Product;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;

/**
 * Class for app which displays the information in an application
 * @author Aaron W Ross
 */
public class App extends Application {
	@SuppressWarnings("unused")
	private Stage primaryStage;
	private Dispenser d = new Dispenser();
	private Product[] products;
	BorderPane root = new BorderPane();
	private static String argFlag = "";
	private static String option = "Queue";
	/**
	 * start method to start the main stage of the app
	 */
	@Override
	public void start(Stage primaryStage) {
		this.primaryStage = primaryStage;
		products = d.initProducts();
		
		//Creating the scene and border for the startup splash
		BorderPane root = new BorderPane();
		Scene startScene = new Scene(root, 900, 500);
		//Making it transparent
		startScene.setFill(Color.TRANSPARENT);
		primaryStage.setScene(startScene);
		primaryStage.show();
		primaryStage.setTitle("Signature Snacks");
		
		//image for the splash screen
		Image splash = new Image("/application/splash.png");
		ImageView imageView1 = new ImageView(splash);
		imageView1.setFitWidth(900);
		imageView1.setFitHeight(500);
		root.setCenter(imageView1);
		
		//Fade transition for the start up splash screen with a 5 second duration 
		FadeTransition ft = new FadeTransition(Duration.millis(5000), imageView1);
		ft.setFromValue(.1);
		ft.setToValue(1.0);
		ft.setCycleCount(1);
		ft.setAutoReverse(false);
		ft.play();
	
		//When transition is done, run either of the scenes
		ft.setOnFinished(e->{
			root.setStyle("-fx-background-color: lightblue");
		
			if(argFlag.equals(option))
			{
				QueueScene qs = new QueueScene(primaryStage, products);
				qs.display(this);
			}
			else
			{
				MainScene ms = new MainScene(primaryStage, products);
				ms.display();
			}
		});
}

	/**
	 * main method to launch app
	 * @param args
	 */
	public static void main(String[] args) {
			if(args.length != 0)
				argFlag = args[0];
			launch(args);
		
	}
	
	/**
	 * Getter for the dispenser
	 * @return
	 */
	public Dispenser getDispenser() {
		return d;
	}
}