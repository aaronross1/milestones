package ms8;
public class Candy extends Snack {
	
	
	/*
	 * Constructor for candy
	 */
	public Candy(Candy c) {
		super(c);
		// TODO Auto-generated constructor stub
	}
	
	/*
	 * Constructor for candy
	 */
	public Candy() {
		super();
	}
	
	/*
	 * Constructor for candy as a subclass of snack and product
	 */
	public Candy(String name, int productID, float price, int stock, String image, String description, int lowQuantity) {
		super(name, productID, price, stock, image, description, lowQuantity);
	}
	
	/*
	 * toString method 
	 */
	public String toString() {
		return super.toString();
	}

}
