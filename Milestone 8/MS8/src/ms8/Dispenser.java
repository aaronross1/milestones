package ms8;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.Scanner;

/**
 * Dispenser class for initializing the array and dealing with checkout
 * @author Aaron W Ross
 *
 */
public class Dispenser {

	@SuppressWarnings("unused")
	private float change;
	@SuppressWarnings("unused")
	private float cashInput;
	private static boolean paymentAccepted;
	private static boolean paymentTypeIsCash;
	static Scanner keyboard = new Scanner(System.in);
	private Product[] products = null;
	
	
	/**
	 * Displaying the checkout questions
	 * @return paymentTypeIsCash
	 */
	public boolean displayCheckout()
	{
		while (paymentAccepted != true) {
			System.out.println(" Payment cash or card? ");
			String paymentType = keyboard.nextLine();
				if (paymentType.equalsIgnoreCase("cash")) {
					paymentTypeIsCash = true;
			paymentAccepted = true;
			System.out.println("Insert cash now");
		}
		
				else if (paymentType.equalsIgnoreCase("card")) {
					paymentTypeIsCash = false;
			paymentAccepted = true;
			System.out.println("Insert card now");
		}
		
				else {
			paymentAccepted = false;
			System.out.println("Invalid type, must be cash or card");
		}
		}
		return paymentTypeIsCash;
	}
	
	
	/**
	 * Set the cash input to dispenser
	 * @param cashInput 
	 */
	public void setPayment(double cashInput)
	{
		
	}
	
	/**
	 * Get payment from customer
	 * @param p 
	 */
	public void getPayment(Product p) {
		if (paymentTypeIsCash = true) {
			System.out.println("How much cash are you putting in?");
			Float cashInput = keyboard.nextFloat();
			if (cashInput > p.getPrice()) {
				
			}
		}
		else if (paymentTypeIsCash = false) {
			paymentAccepted = true;
		}
	}
	
	/**
	 * Dispenser constructor method default
	 */
	public Dispenser() {
		cashInput = 0;
		change = 0;
		paymentAccepted = false;
		paymentTypeIsCash = false;
	}
	
	
	/**
	 * method reading in a file with a product array initializing all products and their data
	 * @return 
	 */
	  public Product[] initProducts() {
		  
		  try
		  (
		     FileReader input = new FileReader("./products.txt");
		     LineNumberReader count = new LineNumberReader(input);
		  )
		  {
		     while (count.skip(Long.MAX_VALUE) > 0)
		     {
		        //empty loop just to count
		     }

		     int result = count.getLineNumber() + 1;            
		     products = new Product[result];
		  }
		  catch (IOException e) {
			  e.printStackTrace();
		  }		  
		  
		  try {
			  int index = 0;
			  String line = "";
			  BufferedReader b = new BufferedReader(new FileReader("./products.txt"));
			  while((line = b.readLine()) != null)
				{
				String[] tokens = line.split("\\|");
				tokens[0] = tokens[0].trim();
				tokens[1] = tokens[1].trim();
				tokens[2] = tokens[2].trim();
				tokens[3] = tokens[3].trim();
				tokens[4] = tokens[4].trim();
				tokens[5] = tokens[5].trim();
				tokens[6] = tokens[6].trim();
				tokens[7] = tokens[7].trim();
				int id = Integer.valueOf(tokens[2]);
				float price = Float.valueOf(tokens[3]);
				int stock = Integer.valueOf(tokens[4]);
				int lowQuantity = Integer.valueOf(tokens[7]);
				
				if(tokens[0].equals("Drink")) {
				products[index] = new Drink(tokens[1], id, price, stock, tokens[5], tokens[6], lowQuantity);
				}
				else if(tokens[0].equals("Candy")) {
				products[index] = new Candy(tokens[1], id, price, stock, tokens[5], tokens[6], lowQuantity);
				}
				else if(tokens[0].equals("Chips")) {
				products[index] = new Chips(tokens[1], id, price, stock, tokens[5], tokens[6], lowQuantity);
				}
				else if(tokens[0].equals("Gum")) {
				products[index] = new Gum(tokens[1], id, price, stock, tokens[5], tokens[6], lowQuantity);
				}
				
				index++;
			  }
			  
		  }
		  catch (IOException e) {
			  e.printStackTrace();
		  } 
			return products;
	  }
	  
	/**
	 * Get price
	 * @param product 
	 * @return 
	 */
	public float getPrice(Product product) {
		return product.getPrice();
	}
	
	/**
	 * Print receipt
	 */
	public void printReceipt() {
		
	}
	
	/**
	 * Set change amount
	 * @param change 
	 */
	public void setChange(float change) {
		
	}
	
	/**
	 * Get the change amount
	 */
	public void getChange() {
		
	}
	
	/**
	 * dispense change method
	 */
	public void dispenseChange() {
		
	}
	
	/**
	 * dispense product method
	 */
	public void dispenseProduct() {
		
	}
	
	public Product[] getProducts() {
		return products;
	}
}
