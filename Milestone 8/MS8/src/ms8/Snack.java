package ms8;
public abstract class Snack extends Product implements Comparable<Product>
{
	/*
	 * Constructor for snack class
	 */
	public Snack() {
		super();
	}
	
	/*
	 * Constructors
	 * @param name productID price stock
	 */
	public Snack(String name, int productID, float price, int stock, String image, String description, int lowQuantity) {
		super(name, productID, price, stock, image, description, lowQuantity);
	}
	/*
	 * Copy Constructor 
	 * @param s
	 */
	public Snack(Snack s) {
		this(s.getName(), s.getProductID(), s.getPrice(), s.getStock(), s.getImage(), s.getDescription(), s.getLowQuantity());
	}
	
	/*
	 * toString method
	 */
	public String toString() {
		return super.toString();
	}

}
