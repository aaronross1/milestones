package MS2;
public class Product {
	private String name;
	private int productID;
	private static float price;
	private int stock;
	protected final String errorMessage = "There is an error. Contact an administrator.";
	
	/*
	 * setter for price
	 */
	public void setPrice(float price) {
		
	}

	/*
	 * getter for price
	 */
	public static float getPrice() {
		return price;
	}
	
	/*
	 * setter for stock
	 */
	public void setStock(int stock) {
		
	}
	
	/*
	 * getter for stock
	 */
	public int getStock() {
		return stock;
	}
	
	/*
	 * setter for product id
	 */
	public void setProductID(int productID) {
		
	}
	
	/*
	 * getter for product id 
	 */
	public int getProductID() {
		return productID;
	}
	
	/*
	 * constructor for product 
	 */
	public Product() {
		
	}
	
	/*
	 * Copy constructor for product 
	 */
	public Product(String name, int productID, float price, int stock) {
		
	}
	
	/*
	 * Product constructor overridden 
	 */
	public Product(Product p) {
		
	}
	
	/*
	 * to string method 
	 */
	public String toString() {
		return toString();
	}
	
	/*
	 * Setter for name
	 */
	public void setName(String name) {
		
	}
	
	/*
	 * Getter for name
	 */
	public String getName() {
		return name;
	}
}
