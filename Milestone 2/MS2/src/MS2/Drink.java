package MS2;
public abstract class Drink extends Product
{

	private int numberOfOunces;
	
	/*
	 * Constructor for drink
	 */
	public Drink(Drink d) {
		super(d);
		// TODO Auto-generated constructor stub
	}
	
	/*
	 * Constructor for drink
	 */
	public Drink() {
		
	}
	
	/*
	 * Constructor for drink as a subclass of snack and product
	 */
	public Drink(String name, int productID, float price, int stock) {
		
	}
	
	/*
	 * toString method 
	 */
	public String toString() {
		return getName();
	}
	
	/*
	 * get method for numberOfOunces
	 */
	public int getNumberOfOunces() {
		return numberOfOunces;
	}
	
	/*
	 * set method for numberOfOunces
	 */
	public void setNumberOfOunces(int numberOfOunces) {
		
	}
}
