package MS2;
import java.util.Scanner;


public class Dispenser {

	private float cashInput;
	private static boolean paymentAccepted;
	private Product product[];
	private static boolean paymentTypeIsCash;
	private float change;
	static Scanner keyboard = new Scanner(System.in);
	
	/*
	 * Main method 
	 * @param String[] args
	 */
	public static void main(String[] args) {
		Chips lays = new Chips();
		Gum spearmint = new Gum();
		Candy snickers = new Candy();
		//Drink coke = new Drink();
		
		//display the products for the user to select
		displayProducts();
		// Display welcome screen, product selection and checkout
		displayCheckout();
		
	}
	
	/**
	 * Displaying the checkout questions
	 * @return paymentTypeIsCash
	 */
	public static boolean displayCheckout()
	{
		while (paymentAccepted != true) {
			System.out.println(" Payment cash or card? ");
			String paymentType = keyboard.nextLine();
				if (paymentType.equalsIgnoreCase("cash")) {
					paymentTypeIsCash = true;
			paymentAccepted = true;
			System.out.println("Insert cash now");
		}
		
				else if (paymentType.equalsIgnoreCase("card")) {
					paymentTypeIsCash = false;
			paymentAccepted = true;
			System.out.println("Insert card now");
		}
		
				else {
			paymentAccepted = false;
			System.out.println("Invalid type, must be cash or card");
		}
		}
		return paymentTypeIsCash;
	}
	
	
	/*
	 * Set the cash input to dispenser
	 */
	public void setPayment(double cashInput)
	{
		
	}
	
	/*
	 * Get payment from customer
	 */
	public void getPayment() {
		if (paymentTypeIsCash = true) {
			System.out.println("How much cash are you putting in?");
			Float cashInput = keyboard.nextFloat();
			if (cashInput > Product.getPrice()) {
				
			}
		}
		else if (paymentTypeIsCash = false) {
			paymentAccepted = true;
		}
	}
	
	/*
	 * Dispenser constructor method
	 */
	public void Dispenser() {
	}
	
	/*
	 * Display machine products
	 */
	public static void displayProducts() {
		System.out.println("Would you like a snack or a drink?");
		String selection = keyboard.nextLine();
		if (selection.equalsIgnoreCase("snack")) {
			System.out.println("Select a product: ");
			System.out.println("Lays:1  Spearmint:2  Snickers:3");
		}
		else if (selection.equalsIgnoreCase("drink")) {
			System.out.println("Select a drink: ");
			System.out.println(" Coke  Dr. Pepper  Sprite");
		}
		// These are temporary objects until we can get the porducts from the other classes working
		String productSelection = keyboard.nextLine();
	}
	
	/*
	 * Get price
	 */
	public float getPrice(Product product) {
		return Product.getPrice();
	}
	
	/*
	 * Print receipt
	 */
	public void printReceipt() {
		
	}
	
	/*
	 * Set change amount
	 */
	public void setChange(float change) {
		
	}
	
	/*
	 * Get the change amount
	 */
	public void getChange() {
		
	}
	
	/*
	 * 
	 */
	public void dispenseChange() {
		
	}
	
	/*
	 * 
	 */
	public void dispenseProduct() {
		
	}
}
