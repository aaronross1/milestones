This is the readme for our milestone project dispenser. 
Our dispenser is a vending machine that outputs drinks and snacks, accepts cash and card, and prints receipts. 
Both team members Aaron and Harpreet completed the code, the UML diagram, the flowchart, and the video together.
Aaron completed the storyboard alone.

Our bitbucket link is as follows; git clone https://aaronross1@bitbucket.org/aaronross1/milestones.git
The link was also shared with your GCU email address if that clone link does not work.

**** BOTH walk through videos are on bit bucket. Not able to submit large files through loud cloud. We did two videos to break up the deliverables into to parts.****