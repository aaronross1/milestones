This is the readme for our milestone project dispenser. 
Our dispenser is a vending machine that outputs drinks and snacks, accepts cash and card, and prints receipts. At start, the program asks for the type of product. You can select snack, drink, or all. Selecting the type will print out all products of that type in alphabetical order. When checking out a product, our vending machine will be able to ask for cash or card, return change or ask for more money, and print out a receipt. 
Both team members Aaron and Harpreet completed the code, the flowchart, and the video was made separately by each.
Aaron completed the storyboard and UML diagram alone

The bitbucket link was also shared with your GCU email address.

**** Walk through Screencast done by Aaron: https://www.useloom.com/share/ec198404b63845578412de7a386fde0c ****