package MS2;
import java.util.Scanner;
import java.util.Arrays;


public class Dispenser {

	private float cashInput;
	private static boolean paymentAccepted;
	private static boolean paymentTypeIsCash;
	private float change;
	static Scanner keyboard = new Scanner(System.in);
	private Product[] products = new Product[10];
	
	
	/*
	 * Main method 
	 * @param String[] args
	 */
	public static void main(String[] args) {
		Dispenser d = new Dispenser();
		d.initProducts();
		d.displayProducts();
		
		//display the products for the user to select
		// Display welcome screen, product selection and checkout
		//displayCheckout();
		

	}
	
	/**
	 * Displaying the checkout questions
	 * @return paymentTypeIsCash
	 */
	public boolean displayCheckout()
	{
		while (paymentAccepted != true) {
			System.out.println(" Payment cash or card? ");
			String paymentType = keyboard.nextLine();
				if (paymentType.equalsIgnoreCase("cash")) {
					paymentTypeIsCash = true;
			paymentAccepted = true;
			System.out.println("Insert cash now");
		}
		
				else if (paymentType.equalsIgnoreCase("card")) {
					paymentTypeIsCash = false;
			paymentAccepted = true;
			System.out.println("Insert card now");
		}
		
				else {
			paymentAccepted = false;
			System.out.println("Invalid type, must be cash or card");
		}
		}
		return paymentTypeIsCash;
	}
	
	
	/*
	 * Set the cash input to dispenser
	 */
	public void setPayment(double cashInput)
	{
		
	}
	
	/*
	 * Get payment from customer
	 */
	public void getPayment(Product p) {
		if (paymentTypeIsCash = true) {
			System.out.println("How much cash are you putting in?");
			Float cashInput = keyboard.nextFloat();
			if (cashInput > p.getPrice()) {
				
			}
		}
		else if (paymentTypeIsCash = false) {
			paymentAccepted = true;
		}
	}
	
	/*
	 * Dispenser constructor method default
	 */
	public Dispenser() {
		this.cashInput = cashInput;
		this.change = change;
		this.paymentAccepted = paymentAccepted;
		this.paymentTypeIsCash = paymentTypeIsCash;
	}
	
	
	/*
	 * Display machine products
	 */
	public void displayProducts() {
		System.out.println("Select Snack, Drink, or all");
		String selection = keyboard.nextLine();
		Arrays.sort(products);
		if(selection.equalsIgnoreCase("drink"))
		{
			for( int i=0; i<products.length; i++) 
			{
				if(products[i] instanceof Drink)
				{
				System.out.println(products[i].toString());
				
				}
			}
		}
		else if(selection.equalsIgnoreCase("snack"))
		{
			for( int i=0; i<products.length; i++) 
			{
				if(products[i] instanceof Snack)
				{
				System.out.println(products[i].toString());
				
				}
			}
		}
		else if(selection.equalsIgnoreCase("all"))
		{
			for(int i = 0; i <10; ++i)
			{
				System.out.println(products[i].toString());
			}
		}
		else 
		{
			System.out.println("Select a valid type");
		}
		
	}
	/**
	 * method with product array initializing all products
	 */
	  private void initProducts() {
		  	products[0] = new Chips("Lays BBQ Chips", 0, (float)1.99, 20);
			products[1] = new Chips("Lays Potato Chips", 1, (float)1.99, 20);
			products[2] = new Chips("Cheetos", 2, (float)1.99, 20);
			products[3] = new Chips("Hot Cheetos", 3, (float)1.99, 20);
			products[4] = new Chips("Ranch Doritos", 4, (float)1.50, 20);
			products[5] = new Chips("Nacho Cheese Doritos", 5, (float)1.99, 20);
			products[6] = new Gum("Juicy Fruit", 6, (float)0.50, 20);
			products[7] = new Drink("Coke", 7, (float)0.99, 20);
			products[8] = new Drink("Dr. Pepper", 8, (float)0.99, 20);
			products[9] = new Candy("Rolos", 9, (float)1.99, 20);
	  }
	/*
	 * Get price
	 */
	public float getPrice(Product product) {
		return product.getPrice();
	}
	
	/*
	 * Print receipt
	 */
	public void printReceipt() {
		
	}
	
	/*
	 * Set change amount
	 */
	public void setChange(float change) {
		
	}
	
	/*
	 * Get the change amount
	 */
	public void getChange() {
		
	}
	
	/*
	 * 
	 */
	public void dispenseChange() {
		
	}
	
	/*
	 * 
	 */
	public void dispenseProduct() {
		
	}
}
