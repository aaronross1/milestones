This is the readme for our milestone project dispenser. 
Our dispenser is a vending machine that outputs drinks and snacks, accepts cash and card, and prints receipts. At start, the program asks for the type of product. You can select snack, or drink. Selecting the type will print out all products of that type. When checking out a product, our vending machine will be able to ask for cash or card, return change or ask for more money, and print out a receipt. 

All work completed by Aaron Ross.

The bitbucket link was also shared with your GCU email address.

**** Walk through Screencast: https://www.useloom.com/share/d6c39f5941a64c37ae76c94971baf96a ****