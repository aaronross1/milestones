package ms4;
import java.util.Scanner;
import java.util.Arrays;

/**
 * Dispenser class for initializing the array and dealing with checkout
 * @author Aaron W Ross
 *
 */
public class Dispenser {

	private float cashInput;
	private static boolean paymentAccepted;
	private static boolean paymentTypeIsCash;
	private float change;
	static Scanner keyboard = new Scanner(System.in);
	private Product[] products = new Product[10];
	
	
	/**
	 * Displaying the checkout questions
	 * @return paymentTypeIsCash
	 */
	public boolean displayCheckout()
	{
		while (paymentAccepted != true) {
			System.out.println(" Payment cash or card? ");
			String paymentType = keyboard.nextLine();
				if (paymentType.equalsIgnoreCase("cash")) {
					paymentTypeIsCash = true;
			paymentAccepted = true;
			System.out.println("Insert cash now");
		}
		
				else if (paymentType.equalsIgnoreCase("card")) {
					paymentTypeIsCash = false;
			paymentAccepted = true;
			System.out.println("Insert card now");
		}
		
				else {
			paymentAccepted = false;
			System.out.println("Invalid type, must be cash or card");
		}
		}
		return paymentTypeIsCash;
	}
	
	
	/**
	 * Set the cash input to dispenser
	 */
	public void setPayment(double cashInput)
	{
		
	}
	
	/**
	 * Get payment from customer
	 */
	public void getPayment(Product p) {
		if (paymentTypeIsCash = true) {
			System.out.println("How much cash are you putting in?");
			Float cashInput = keyboard.nextFloat();
			if (cashInput > p.getPrice()) {
				
			}
		}
		else if (paymentTypeIsCash = false) {
			paymentAccepted = true;
		}
	}
	
	/**
	 * Dispenser constructor method default
	 */
	public Dispenser() {
		cashInput = 0;;
		change = 0;
		paymentAccepted = false;
		paymentTypeIsCash = false;
	}
	
	
	/**
	 * method with product array initializing all products
	 */
	  public Product[] initProducts() {
		  	products[0] = new Chips("Lays BBQ Chips", 0, (float)1.99, 20, "./images/bbq.jpg", "Classic Lays BBQ chips.");
			products[1] = new Chips("Lays Potato Chips", 1, (float)1.99, 20, "./images/lays.jpg", "Classic Lays potato chips");
			products[2] = new Chips("Cheetos", 2, (float)1.99, 20, "./images/cheetos.jpg", "Cheetos, one cheesy crunch.");
			products[3] = new Chips("Hot Cheetos", 3, (float)1.99, 20, "./images/hotcheetos.jpg", "Flamin' hot cheesy crunches.");
			products[4] = new Chips("Ranch Doritos", 4, (float)1.50, 20, "./images/ranch.jpg", "No need for ranch dip with these delicious chips.");
			products[5] = new Chips("Nacho Cheese Doritos", 5, (float)1.99, 20, "./images/nacho.jpg", "The cheese on this is Nacho Cheese, it's mine.");
			products[6] = new Gum("Juicy Fruit", 6, (float)0.50, 20, "./images/juicy.jpg", "Juicy Fruit gum to refresh your breath.");
			products[7] = new Drink("Coke", 7, (float)0.99, 20, "./images/coke.jpg", "A cold refreshing classic Coke.");
			products[8] = new Drink("Dr. Pepper", 8, (float)0.99, 20, "./images/drpepper.jpg", "Dr. Pepper, because the Dr. is never wrong.");
			products[9] = new Candy("Rolos", 9, (float)1.99, 20, "./images/rolos.jpg", "A treat you can savor.");
			
			return products;
	  }
	/**
	 * Get price
	 */
	public float getPrice(Product product) {
		return product.getPrice();
	}
	
	/**
	 * Print receipt
	 */
	public void printReceipt() {
		
	}
	
	/**
	 * Set change amount
	 */
	public void setChange(float change) {
		
	}
	
	/**
	 * Get the change amount
	 */
	public void getChange() {
		
	}
	
	/**
	 * dispense change method
	 */
	public void dispenseChange() {
		
	}
	
	/**
	 * dispense product method
	 */
	public void dispenseProduct() {
		
	}
}
