package ms4;
public class Gum extends Snack
{
	/*
	 * Constructor for gum
	 */
	public Gum(Gum g) {
		super(g);
		// TODO Auto-generated constructor stub
	}
	
	/*
	 * Constructor for gum
	 */
	public Gum() {
		super();
	}
	
	/*
	 * Constructor for gum as a subclass of snack and product
	 */
	public Gum(String name, int productID, float price, int stock, String image, String description) {
		super(name, productID, price, stock, image, description);
	}
	
	/*
	 * toString method 
	 */
	public String toString() {
		return super.toString();
	}

}
