package ms4;
public class Drink extends Product implements Comparable<Product>
{

	private int numberOfOunces;
	
	/*
	 * Constructor for drink
	 */
	public Drink(Drink d) {
		this(d.getName(), d.getProductID(), d.getPrice(), d.getStock(), d.getImage(), d.getDescription());
		
	}
	
	/*
	 * Constructor for drink
	 */
	public Drink() {
		super();
	}
	
	/*
	 * Constructor for drink as a subclass of snack and product
	 */
	public Drink(String name, int productID, float price, int stock, String image, String description) {
		super(name, productID, price, stock, image, description);
	}
	
	/*
	 * toString method 
	 */
	public String toString() {
		
		return super.toString();
	}
	
	/*
	 * get method for numberOfOunces
	 */
	public int getNumberOfOunces() {
		return numberOfOunces;
	}
	
	/*
	 * set method for numberOfOunces
	 */
	public void setNumberOfOunces(int numberOfOunces) {
		
	}
	

}
