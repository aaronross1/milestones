package application;
import ms4.Product;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import ms4.Product;

/**
 * Display product class which extends vbox
 * @author Aaron W Ross
 *
 */
public class DisplayProduct extends VBox {
	private Product product;
	
	/**
	 * Display product non-default constructor
	 * @param product
	 */
	public DisplayProduct(Product product) { 
		this.product = product;
		
	}
	
	/**
	 * show product method with the vbox return type
	 * @return
	 */
	public VBox showProduct() {
		Button btn1 = new Button("Select");
		btn1.setStyle("-fx-background-color: white");
		btn1.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.REGULAR,20));
		
		Image productImage = new Image(product.getImage());
		ImageView imageView1 = new ImageView(productImage);
		imageView1.setFitWidth(100);
		imageView1.setFitHeight(100);

		Text description = new Text(product.getDescription());
		description.setFill(Color.BLACK);
		description.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.REGULAR, 14));

		
		Text price = new Text(product.toString());
		price.setFill(Color.BLACK);
		price.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.REGULAR, 14));

		VBox vbox = new VBox();
		vbox.setPadding(new Insets(10, 10, 10, 10));
		vbox.getChildren().addAll(imageView1, description, price, btn1);
		vbox.setSpacing(10);
		vbox.setAlignment(Pos.CENTER);
		return vbox;
	}
}
