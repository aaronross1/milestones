package application;
import ms4.Dispenser;
import ms4.Drink;
import ms4.Snack;
import application.DisplayProduct;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.stage.Stage;
import ms4.Dispenser;
import ms4.Product;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

/**
 * Class for app which displays the information in an application
 * @author Aaron W Ross
 */
public class App extends Application {
	private Stage primaryStage;
	private Dispenser d = new Dispenser();
	private Product[] products;
	
	/**
	 * start method to start the main stage of the app
	 */
	@Override
	public void start(Stage primaryStage) {
		this.primaryStage = primaryStage;
		products = d.initProducts();
		try {
			mainScene(primaryStage);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * main method to launch app
	 * @param args
	 */
	public static void main(String[] args) {
		launch(args);
	}

	/**
	 * mainStage method creating the main menu
	 * 
	 * @param primaryStage
	 */
	public void mainScene(Stage primaryStage) {
		BorderPane root = new BorderPane();
		Scene scene = new Scene(root, 800, 900);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());

		primaryStage.setTitle("Signature Snacks");
		root.setStyle("-fx-background-color: lightblue");
		Text header = new Text();
		header.setText("Signature Snacks");
		header.setFill(Color.WHITE);
		header.setFont(Font.font("Calibri", FontWeight.BOLD, FontPosture.REGULAR, 50));
		BorderPane.setAlignment(header, Pos.CENTER);
		root.setTop(header);

		Text footer = new Text();
		footer.setText(" Developed by Aaron W Ross ");
		footer.setFill(Color.WHITE);
		footer.setFont(Font.font("Calibri", FontWeight.BLACK, FontPosture.REGULAR, 16));
		BorderPane.setAlignment(footer, Pos.CENTER);
		root.setBottom(footer);

		root.setCenter(createMenu());
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	/**
	 * creating the menu buttons for snack and drink using hbox
	 * 
	 * @return hbox
	 */
	public HBox createMenu() {
		Button btn1 = new Button("Snacks");
		btn1.setStyle("-fx-background-color: white");
		btn1.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.REGULAR, 35));
		btn1.setOnAction(new ButtonClickHandlerSnack());

		Button btn3 = new Button("Drinks");
		btn3.setStyle("-fx-background-color: white");
		btn3.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.REGULAR, 35));
		btn3.setOnAction(new ButtonClickHandlerDrink());

		HBox hbox = new HBox();
		hbox.setPadding(new Insets(90, 90, 90, 90));
		hbox.getChildren().addAll(btn1, btn3);
		hbox.setSpacing(50);
		hbox.setAlignment(Pos.CENTER);
		return hbox;
	}

	/*
	 * 
	 * drink stage for the drink menu
	 */
	public void drinkScene(Stage primaryStage) {
		BorderPane root = new BorderPane();
		Scene scene = new Scene(root, 900, 900);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());

		root.setStyle("-fx-background-color: lightblue");
		Text header = new Text();
		header.setText("Select a snack");
		header.setFill(Color.WHITE);
		header.setFont(Font.font("Calibri", FontWeight.BOLD, FontPosture.REGULAR, 50));
		BorderPane.setAlignment(header, Pos.CENTER);
		root.setTop(header);

		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(10);
		grid.setVgap(10);
		
		int row = 0;
		int column = 0;
		for( int i=0; i<products.length; i++) 
		{
			if(products[i] instanceof Drink)
			{
				grid.add(new DisplayProduct(products[i]).showProduct(), column, row);
				column++;
				if(column == 3)
				{
					column = 0;
					row++;
				}
			}
		}
		BorderPane.setAlignment(grid, Pos.CENTER);
		root.setCenter(grid);
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	/**
	 * Snack stage for displaying the snacks
	 * @param primaryStage
	 */
	public void snackScene(Stage primaryStage) {
		BorderPane root = new BorderPane();
		Scene scene = new Scene(root, 900, 900);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());

		root.setStyle("-fx-background-color: lightblue");
		Text header = new Text();
		header.setText("Select a snack");
		header.setFill(Color.WHITE);
		header.setFont(Font.font("Calibri", FontWeight.BOLD, FontPosture.REGULAR, 50));
		BorderPane.setAlignment(header, Pos.CENTER);
		root.setTop(header);

		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(10);
		grid.setVgap(10);
		
		int row = 0;
		int column = 0;
		for( int i=0; i<products.length; i++) 
		{
			if(products[i] instanceof Snack)
			{
				grid.add(new DisplayProduct(products[i]).showProduct(), column, row);
				column++;
				if(column == 3)
				{
					column = 0;
					row++;
				}
			}
		}
		BorderPane.setAlignment(grid, Pos.CENTER);
		root.setCenter(grid);
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	/**
	 * Button click handler for snack button on the main menu
	 */
	class ButtonClickHandlerSnack implements EventHandler<ActionEvent> {
		@Override
		public void handle(ActionEvent e) {
			snackScene(primaryStage);
		}
	}

	/**
	 * Button click handler for drink button on the main menu
	 */
	class ButtonClickHandlerDrink implements EventHandler<ActionEvent> {
		@Override
		public void handle(ActionEvent e) {
			drinkScene(primaryStage);
		}
	}
}
