package ms5;
public class Chips extends Snack {

	/*
	 * Constructor for chips
	 */
	public Chips(Chips c) {
		super(c);
		// TODO Auto-generated constructor stub
	}
	
	/*
	 * Constructor for chips
	 */
	public Chips() {
		super();
	}
	
	/*
	 * Constructor for chips as a subclass of snack and product
	 */
	public Chips(String name, int productID, float price, int stock, String image, String description) {
		super(name, productID, price, stock, image, description);
	}
	
	/*
	 * toString method 
	 */
	public String toString() {
		return super.toString();
	}

}
