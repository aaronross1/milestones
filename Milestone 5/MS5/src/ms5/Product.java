package ms5;

import java.text.DecimalFormat;

/**
 * abstract class for product
 * @author Aaron W Ross
 *
 */
public abstract class Product implements Comparable<Product>{
	protected String name;
	private int productID;
	private float price;
	private int stock;
	private String image;
	private String description;
	protected final String errorMessage = "There is an error. Contact an administrator.";

	
	/**
	 * setter for price
	 * @param price 
	 */
	public void setPrice(float price) {

	}

	/**
	 * getter for price
	 * @return 
	 */
	public float getPrice() {
		return price;
	}


	/**
	 * setter for stock
	 * @param stock
	 */
	public void setStock(int stock) {
		this.stock = stock;
	}

	/**
	 * getter for stock
	 * @return 
	 */
	public int getStock() {
		return stock;
	}

	/**
	 * setter for product id
	 * @param productID 
	 */
	public void setProductID(int productID) {

	}

	/**
	 * getter for product id
	 * @return 
	 */
	public int getProductID() {
		return productID;
	}

	/**
	 * Setter for image
	 * @param image 
	 */
	public void setImage(String image) {
		
	}
	
	/**
	 * getter for product image
	 * @return 
	 */
	public String getImage() {
		return image;
	}
	
	/**
	 * Setter for image
	 * @param description 
	 */
	public void setDescription(String description) {
		
	}
	
	/**
	 * getter for product image
	 * @return 
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * constructor for product
	 */
	public Product() {
	
	}

	/**
	 * Copy constructor for product
	 * @param name 
	 * @param productID 
	 * @param price 
	 * @param stock 
	 * @param image 
	 * @param description 
	 */
	public Product(String name, int productID, float price, int stock, String image, String description) {
		this.name = name;
		this.price = price;
		this.productID = productID;
		this.stock = stock;
		this.image = image;
		this.description = description;

	}

	/**
	 * Product constructor overridden
	 * @param p 
	 */
	public Product(Product p) {
		this(p.getName(), p.getProductID(), p.getPrice(), p.getStock(), p.getImage(), p.getDescription());
	}

	/**
	 * to string method
	 */
	public String toString() {
		DecimalFormat df = new DecimalFormat("#.00");
		return this.name + " - $" + df.format(this.price);
	}

	/**
	 * Setter for name
	 * @param name 
	 */
	public void setName(String name) {

	}

	/**
	 * Getter for name
	 * @return 
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * compare to method to compare the names and sort by them.
	 */
	public int compareTo(Product p) {
		// TODO Auto-generated method stub
		int value = this.name.compareToIgnoreCase(p.name);
		return value;
	}
}
