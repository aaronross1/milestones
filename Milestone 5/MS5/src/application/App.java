package application;
import javafx.application.Application;
import javafx.stage.Stage;
import ms5.Dispenser;
import ms5.Product;
import javafx.scene.layout.BorderPane;

/**
 * Class for app which displays the information in an application
 * @author Aaron W Ross
 */
public class App extends Application {
	@SuppressWarnings("unused")
	private Stage primaryStage;
	private Dispenser d = new Dispenser();
	private Product[] products;
	BorderPane root = new BorderPane();
	
	/**
	 * start method to start the main stage of the app
	 */
	@Override
	public void start(Stage primaryStage) {
		this.primaryStage = primaryStage;
		products = d.initProducts();
		try {
			MainScene ms = new MainScene(primaryStage, products);
			ms.display();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * main method to launch app
	 * @param args
	 */
	public static void main(String[] args) {
		launch(args);
	}
}