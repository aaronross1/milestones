/**
 * 
 */
package application;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import ms5.Drink;
import ms5.Product;

/**
 * Drink scene which houses the output of all drink products
 * @author Aaron W Ross
 *
 */
public class DrinkScene {

	Stage primaryStage;
	Product [] products;
	SceneUtilities su;

	/**
	 * DrinkScene non-default constructor
	 * @param primaryStage 
	 * @param products 
	 * @param root
	 */
	public DrinkScene(Stage primaryStage, Product[] products) {
		this.su = new SceneUtilities(primaryStage, products);
		this.primaryStage = primaryStage;
		this.products = products;
	}

	/**
	 * Display method for the drink scene
	 */
	public void display() {
		BorderPane root = new BorderPane();
		Scene scene = new Scene(root, 900, 900);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		root.setStyle("-fx-background-color: lightblue");
		Text header = new Text();
		header.setText("Select a snack");
		header.setFill(Color.WHITE);
		header.setFont(Font.font("Calibri", FontWeight.BOLD, FontPosture.REGULAR, 50));
		BorderPane.setAlignment(header, Pos.CENTER);

		//home button
		root.setTop(su.header());
		
		//Checkout cart button and logo
		root.setBottom(su.checkoutFooter());
		BorderPane.setAlignment(su.checkoutFooter(), Pos.BOTTOM_RIGHT);
		
		
		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(10);
		grid.setVgap(10);
		
		int row = 0;
		int column = 0;
		for( int i=0; i<products.length; i++) 
		{
			if(products[i] instanceof Drink)
			{
				ProductView pv = new ProductView(products[i]);
				grid.add(pv.showProduct("customer"), column, row);
				column++;
				if(column == 3)
				{
					column = 0;
					row++;
				}
			}
		}
		BorderPane.setAlignment(grid, Pos.CENTER);
		root.setCenter(grid);
		
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
}
