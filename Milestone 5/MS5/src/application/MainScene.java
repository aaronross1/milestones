/**
 * 
 */
package application;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import ms5.Product;

/**
 * class which houses the main home page scene
 * @author Aaron W Ross
 *
 */
public class MainScene {

	Stage primaryStage;
	Product[] products;
	SceneUtilities su;

	/**
	 * MainScene non-default constructor
	 * @param primaryStage
	 * @param products
	 * @param root
	 */
	public MainScene(Stage primaryStage, Product[] products) {
		this.su = new SceneUtilities(primaryStage, products);
		this.primaryStage = primaryStage;
		this.products = products;

	}

	/**
	 * display method creating the main menu and home scene
	 * 
	 * @param primaryStage
	 */
	public void display() {
		BorderPane root = new BorderPane();
		Scene scene = new Scene(root, 800, 900);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());

		primaryStage.setTitle("Signature Snacks");
		root.setStyle("-fx-background-color: lightblue");

		Text header = new Text();
		header.setText("Signature Snacks");
		header.setFill(Color.WHITE);
		header.setFont(Font.font("Calibri", FontWeight.BOLD, FontPosture.REGULAR, 50));
		BorderPane.setAlignment(header, Pos.CENTER);
		root.setTop(header);

		Text footer = new Text();
		footer.setText(" Developed by Aaron W Ross ");
		footer.setFill(Color.WHITE);
		footer.setFont(Font.font("Calibri", FontWeight.BLACK, FontPosture.REGULAR, 16));
		BorderPane.setAlignment(footer, Pos.CENTER);
		root.setBottom(footer);

		scene.addEventHandler(KeyEvent.KEY_PRESSED, event -> new ButtonClickHandlerAdmin(scene));
		
		root.setCenter(createMenu());

		primaryStage.setScene(scene);
		primaryStage.show();
	}

	/**
	 * creating the menu buttons for snack and drink using hbox
	 * 
	 * @return hbox
	 */
	public HBox createMenu() {
		Button btn1 = new Button("Snacks");
		btn1.setStyle("-fx-background-color: white");
		btn1.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.REGULAR, 35));
		btn1.setOnAction(new ButtonClickHandlerSnack());

		Button btn3 = new Button("Drinks");
		btn3.setStyle("-fx-background-color: white");
		btn3.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.REGULAR, 35));
		btn3.setOnAction(new ButtonClickHandlerDrink());

		HBox hbox = new HBox();
		hbox.setPadding(new Insets(90, 90, 90, 90));
		hbox.getChildren().addAll(btn1, btn3);
		hbox.setSpacing(50);
		hbox.setAlignment(Pos.CENTER);
		return hbox;
	}

	/**
	 * Keyboard pressed handler for admin panel on the home screen
	 */
	class ButtonClickHandlerAdmin {

		public ButtonClickHandlerAdmin(Scene a) {
			a.addEventFilter(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
				public void handle(KeyEvent a) {
					if (a.getCode() == KeyCode.A) {

						AdminScene as = new AdminScene(primaryStage, products);
						as.display();
					}

				}
			});
		}
	}

	/**
	 * Button click handler for snack button on the main menu
	 */
	class ButtonClickHandlerSnack implements EventHandler<ActionEvent> {
		@Override
		public void handle(ActionEvent e) {
			SnackScene ss = new SnackScene(primaryStage, products);
			ss.display();
		}
	}

	/**
	 * Button click handler for drink button on the main menu
	 */
	class ButtonClickHandlerDrink implements EventHandler<ActionEvent> {
		@Override
		public void handle(ActionEvent e) {
			DrinkScene ds = new DrinkScene(primaryStage, products);
			ds.display();
		}
	}
}
