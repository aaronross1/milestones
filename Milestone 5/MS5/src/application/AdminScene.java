/**
 * 
 */
package application;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import ms5.Product;

/**
 * @author Aaron W Ross
 *
 */
public class AdminScene {

	Stage primaryStage;
	Product[] products;
	SceneUtilities su;

	/**
	 * AdminScene non-default constructor
	 * @param primaryStage 
	 * @param products 
	 * @param root
	 */
	public AdminScene(Stage primaryStage, Product[] products) {
		this.su = new SceneUtilities(primaryStage, products);
		this.primaryStage = primaryStage;
		this.products = products;
	}

	/**
	 * Display method for the admin scene
	 */
	public void display() {
		BorderPane root = new BorderPane();
		Scene scene = new Scene(root, 900, 900);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());

		root.setStyle("-fx-background-color: lightblue");
		Text header = new Text();
		header.setText("Select a snack");
		header.setFill(Color.WHITE);
		header.setFont(Font.font("Calibri", FontWeight.BOLD, FontPosture.REGULAR, 50));
		BorderPane.setAlignment(header, Pos.CENTER);

		// home button and all products
		root.setTop(su.adminHeader());

		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(10);
		grid.setVgap(10);

		int row = 0;
		int column = 0;
		for (int i = 0; i < products.length; i++) {
			ProductView pv = new ProductView(products[i]);
			grid.add(pv.showProduct("admin"), column, row);
			column++;
			if (column == 3) {
				column = 0;
				row++;
			}
		}

		ScrollPane scroll = new ScrollPane(grid);
		scroll.setStyle("-fx-background-color: lightblue");
		scroll.setFitToHeight(true);
		scroll.setFitToWidth(true);
		BorderPane.setAlignment(grid, Pos.CENTER);
		root.setCenter(scroll);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
}
