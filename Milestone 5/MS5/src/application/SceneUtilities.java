/**
 * 
 */
package application;

import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;
import ms5.Product;

/**
 * class that houses headers and cart animation effect
 * @author Aaron W Ross
 *
 */
public class SceneUtilities {

	Stage primaryStage;
	Product [] products;
	
	/**
	 *  Non-default constructor for SceneUtilities
	 * @param primaryStage
	 * @param products
	 */
	public SceneUtilities(Stage primaryStage, Product[] products) {
		this.primaryStage = primaryStage;
		this.products = products;

	}

	/**
	 * Header with home button and header title
	 * 
	 * @return
	 */
	public HBox header() {
		Button btn1 = new Button("Home");
		btn1.setStyle("-fx-background-color: white");
		btn1.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.REGULAR, 30));
		btn1.setOnAction(new ButtonClickHandlerHome());

		Text header = new Text();
		header.setText("Select a product");
		header.setFill(Color.WHITE);
		header.setFont(Font.font("Calibri", FontWeight.BOLD, FontPosture.REGULAR, 50));

		HBox hbox = new HBox();
		hbox.setPadding(new Insets(0, 0, 0, 120));
		hbox.getChildren().addAll(btn1, header);
		hbox.setSpacing(50);
		hbox.setAlignment(Pos.TOP_LEFT);
		return hbox;

	}

	/**
	 * Returns an hbox with the header for the admin scene
	 * @return 
	 * 
	 */
	public HBox adminHeader() {
		Button btn1 = new Button("Home");
		btn1.setStyle("-fx-background-color: white");
		btn1.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.REGULAR, 30));
		btn1.setOnAction(new ButtonClickHandlerHome());

		Button btn2 = new Button("Restock All");
		btn2.setStyle("-fx-background-color: white");
		btn2.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.REGULAR, 20));
		btn2.setOnAction(new RestockEffect(btn2));



		Text header = new Text();
		header.setText("Admin Panel");
		header.setFill(Color.WHITE);
		header.setFont(Font.font("Calibri", FontWeight.BOLD, FontPosture.REGULAR, 50));

		HBox hbox = new HBox();
		hbox.setPadding(new Insets(0, 0, 0, 120));
		hbox.getChildren().addAll(btn1, header, btn2);
		hbox.setSpacing(50);
		hbox.setAlignment(Pos.TOP_LEFT);
		return hbox;

	}

	/**
	 * checkout button and cart image
	 * 
	 * @return
	 */
	public HBox checkoutFooter() {

		Image cart = new Image("./images/cart.png");
		ImageView imageView = new ImageView(cart);
		imageView.setFitWidth(50);
		imageView.setFitHeight(50);

		Button btn2 = new Button("Cancel Selection");
		btn2.setStyle("-fx-background-color: white");
		btn2.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.REGULAR, 20));
		btn2.setOnAction(new CancelSelection());
		
		Button btn1 = new Button("Checkout");
		btn1.setStyle("-fx-background-color: white");
		btn1.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.REGULAR, 20));
		btn1.setOnAction(new CartEffect(imageView));

		HBox hbox = new HBox();
		hbox.setPadding(new Insets(20, 60, 30, 0));
		hbox.getChildren().addAll(btn2, btn1, imageView);
		hbox.setSpacing(20);
		hbox.setAlignment(Pos.BOTTOM_RIGHT);
		return hbox;
	}

	
	/**
	 * Button click handler for the home button to go home. 
	 * @author Aaron W Ross
	 *
	 */
	class ButtonClickHandlerHome implements EventHandler<ActionEvent> {
		@Override
		public void handle(ActionEvent e) {
			MainScene ms = new MainScene(primaryStage, products);
			ms.display();
		}
	}

	/**
	 * event handler for animation effect for cart
	 * 
	 * @author Aaron W Ross
	 *
	 */
	class CartEffect implements EventHandler<ActionEvent> {

		private ImageView imageView;

		/**
		 * Click handler effect for the cart imageView
		 * @param imageView
		 */
		public CartEffect(ImageView imageView) {
			this.imageView = imageView;
		}

		/**
		 * handler for the action of "bouncing" the cart
		 */
		@Override
		public void handle(ActionEvent e) {

			TranslateTransition bounce = new TranslateTransition(Duration.millis(50), imageView);
			bounce.setFromX(0f);
			bounce.setByX(10F);
			bounce.setCycleCount(4);
			bounce.setAutoReverse(true);
			bounce.play();
		}
	}
	
	/**
	 * event handler for restock button and effect
	 * 
	 * @author Aaron W Ross
	 *
	 */
	class RestockEffect implements EventHandler<ActionEvent> {

		private Button btn2;

		/**
		 * Click handler effect for the restock button
		 * @param imageView
		 */
		public RestockEffect(Button btn2) {
			this.btn2 = btn2;
		}

		/**
		 * handler for the action of "bouncing" the cart
		 */
		@Override
		public void handle(ActionEvent e) {

			for (int i = 0; i < products.length; i++) {
				if (products[i].getStock() < 20) {
				products[i].setStock(20);
				AdminScene as = new AdminScene(primaryStage, products);
				as.display();
				}
			}
			
			TranslateTransition bounce = new TranslateTransition(Duration.millis(50), btn2);
			bounce.setFromX(0f);
			bounce.setByX(10F);
			bounce.setCycleCount(4);
			bounce.setAutoReverse(true);
			bounce.play();
		}
	}

	/**
	 * event handler for restock button and effect
	 * 
	 * @author Aaron W Ross
	 *
	 */
	class CancelSelection implements EventHandler<ActionEvent> {
		/**
		 * handler for the action of "bouncing" the cart
		 */
		@Override
		public void handle(ActionEvent e) {

			for (int i = 0; i < products.length; i++) {
				if (products[i].getStock() < 20) {
				products[i].setStock(20);
				}
			}

		}
	}
}
