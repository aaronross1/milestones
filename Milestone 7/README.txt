This is the readme for my milestone project dispenser. 
User guide: On startup, you're greeted with a choice of snacks or products. When you select either, you are taken to a screen listing all of the products of that type. You can select any product you like using the select button. If the that product is out of stock, the select button will shake, signifying that the selection did not work. After making a selection, the user will be able to choose the checkout button to go to the checkout screen. The user can select the home button to go home at any time, however this does not cancel the selection.
User guide for admins: On startup, you see the main screen which the ordinary user sees. However, instead of pressing one of the two options, the you may press the "a" key twice to go the admin screen. When you do this, you can see all of the products with the amount of them in stock. Selecting the home button will take you back to the main screen. There is a search button on the admin screen which allows you to search for products in  your inventory. Only the exact matches to the search will return a product. Finally, selecting the restock all button will take the user to a restock ticket which displays an order of all the products restocked, the number restocked, and the total price. 


My dispenser is a vending machine that outputs drinks and snacks, accepts cash and card, and prints receipts. At start, the program asks for the type of product. You can select snack, or drink. Selecting the type will print out all products of that type. After selection, you can cancel an order or click checkout to go to the checkout screen. When checking out a product, our vending machine will be able to ask for cash or card, return change or ask for more money, and print out a receipt. 

If you run the program with "Queue" entered in as an argument, you will be greeted with a screen showing a queue of items that have been ordered by customers. This has been read from a text file and dynamically animates the screen as it reads the file. It reads it so quickly, however, that you are unable to see it dynamically populating the screen with the orders. If an order from the text file doesn't match a product in the inventory, an alternate product will be ordered.

All work completed by Aaron Ross.

The bitbucket link was also shared with your GCU email address.

**** Walk through Screencast: https://www.useloom.com/share/ae0c53076ddb4c228ba06fbde2445e24 ****